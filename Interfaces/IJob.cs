﻿using System.Threading.Tasks;

namespace StudioKit.Cloud.Jobs.Interfaces
{
	public interface IJob
	{
		/// <summary>
		/// Executes the job.
		/// </summary>
		Task ExecuteAsync();
	}
}