﻿using Autofac;
using StudioKit.Cloud.Jobs.Exceptions;
using StudioKit.Cloud.Jobs.Interfaces;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Jobs
{
	/// <summary>
	/// Top level <see cref="RoleEntryPoint"/> for WorkerRoles.
	/// </summary>
	public abstract class JobRoleEntryPoint : AutofacRoleEntryPoint
	{
		/// <summary>
		/// Provides a <see cref="CancellationToken"/> for the top level function <see cref="RoleEntryPoint.Run"/>.
		/// </summary>
		private readonly CancellationTokenSource _cancellationTokenSource;

		private readonly ManualResetEvent _runCompleteEvent;
		private readonly int _defaultConnectionLimit;

		/// <summary>
		/// Name of the Role to use in Log and Exception Messages.
		/// </summary>
		protected readonly string RoleName;

		/// <summary>
		/// Amount of milliseconds between each <see cref="RoleEntryPoint.Run"/> iteration. Default is 1 second.
		/// </summary>
		protected const int RunIntervalMillis = 1000;

		protected ILogger Logger { get; set; }

		protected IErrorHandler ErrorHandler { get; set; }

		protected JobRoleEntryPoint() : this("JobRole", null)
		{
		}

		protected JobRoleEntryPoint(string roleName, int? defaultConnectionLimit)
		{
			RoleName = roleName;
			_defaultConnectionLimit = defaultConnectionLimit ?? 12;
			_cancellationTokenSource = new CancellationTokenSource();
			_runCompleteEvent = new ManualResetEvent(false);
		}

		protected override ContainerBuilder CreateAndConfigureBuilder()
		{
			var builder = new ContainerBuilder();
			RegisterServices(builder);
			return builder;
		}

		protected override void RegisterServices(ContainerBuilder builder)
		{
			// Singletons
			Logger = new Logger("JobRoleLogger");
			builder.Register(c => Logger)
				.As<ILogger>()
				.SingleInstance();
			ErrorHandling.ErrorHandler.Instance = new SentryErrorHandler(
				EncryptedConfigurationManager.GetSetting("SentryDSN"),
				new DefaultJsonPacketFactory());
			ErrorHandler = ErrorHandling.ErrorHandler.Instance;
			builder.RegisterInstance(ErrorHandling.ErrorHandler.Instance)
				.As<IErrorHandler>()
				.ExternallyOwned();
		}

		protected override bool OnRoleStarted()
		{
			var result = base.OnRoleStarted();
			ServicePointManager.DefaultConnectionLimit = _defaultConnectionLimit;
			Logger.Info($"{RoleName} has started");
			return result;
		}

		protected override void OnRoleStopping()
		{
			Logger.Info($"{RoleName} is stopping");
			_cancellationTokenSource.Cancel();
			_runCompleteEvent.WaitOne();
		}

		protected override void OnRoleStopped()
		{
			Logger.Info($"{RoleName} has stopped");
		}

		public override void Run()
		{
			try
			{
				Logger.Debug($"{RoleName} is running");
				RunAsync(_cancellationTokenSource.Token).Wait();
			}
			catch (Exception e)
			{
				var message = $"{RoleName} crashed: {e.Message}";
				Logger.Critical(message);
				ErrorHandler.CaptureException(new JobRoleException(message, e));
			}
			finally
			{
				_runCompleteEvent.Set();
			}
		}

		protected virtual async Task RunAsync(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				try
				{
					foreach (var job in Jobs)
					{
						await job.ExecuteAsync();
					}
				}
				catch (Exception e)
				{
					var message = $"{RoleName} failed: {e}";
					Logger.Error(message);
					ErrorHandler.CaptureException(new JobRoleException(message, e));
				}
				cancellationToken.WaitHandle.WaitOne(RunIntervalMillis);
			}
		}

		protected abstract IEnumerable<IJob> Jobs { get; }
	}
}