﻿using Autofac;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace StudioKit.Cloud.Jobs
{
	/// <summary>
	/// Abstract implementation of a autofac-capable <see cref="RoleEntryPoint"/> for Azure roles.
	/// </summary>
	public abstract class AutofacRoleEntryPoint : RoleEntryPoint
	{
		protected IContainer Container { get; set; }

		/// <summary>
		/// Called by Windows Azure to initialize the role instance.
		/// </summary>
		/// <returns>
		/// True if initialization succeeds, False if it fails. The default implementation returns True.
		/// </returns>
		public sealed override bool OnStart()
		{
			// Autofac configuration
			var builder = CreateAndConfigureBuilder();
			Container = builder.Build();

			return OnRoleStarted();
		}

		/// <summary>
		/// Called by Windows Azure after the role instance has been initialized. This method serves as the
		/// main thread of execution for your role.
		/// </summary>
		public abstract override void Run();

		/// <summary>
		/// Called by Windows Azure when the role instance is to be stopped.
		/// </summary>
		public sealed override void OnStop()
		{
			OnRoleStopping();

			Container?.Dispose();
			Container = null;

			OnRoleStopped();
		}

		/// <summary>
		/// The extension point to create the builder and load all modules for your Azure role.
		/// </summary>
		/// <returns>The Autofac ContainerBuilder</returns>
		protected abstract ContainerBuilder CreateAndConfigureBuilder();

		/// <summary>
		/// The extension point to bind services to the builder.
		/// </summary>
		/// <param name="builder"></param>
		protected abstract void RegisterServices(ContainerBuilder builder);

		/// <summary>
		/// The extension point to perform custom startup actions for your Azure role.
		/// This method is called after the Autofac container is created.
		/// </summary>
		/// <returns>True if startup succeeds, False if it fails. The default implementation returns True.</returns>
		protected virtual bool OnRoleStarted()
		{
			return true;
		}

		/// <summary>
		/// The extension point to perform custom shutdown actions that needs Autofac container.
		/// This method is called before the Autofac container is disposed.
		/// </summary>
		protected virtual void OnRoleStopping() { }

		/// <summary>
		/// The extension point to perform custom shutdown actions for your Azure role.
		/// This method is called after the Autofac container is disposed.
		/// </summary>
		protected virtual void OnRoleStopped() { }
	}
}