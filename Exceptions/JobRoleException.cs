﻿using System;

namespace StudioKit.Cloud.Jobs.Exceptions
{
	public class JobRoleException : Exception
	{
		public JobRoleException()
		{
		}

		public JobRoleException(string message)
			: base(message) { }

		public JobRoleException(string message, Exception inner)
			: base(message, inner) { }
	}
}